import { doc, getDocs, collection, writeBatch } from 'firebase/firestore';

import { db } from '../src/firebase/index.js';
import beerData from '../src/data/beers.js';

const beers = beerData();

const collectionsArray = Object.keys(beers).map((key) => ({
  ...beers[key],
  beers: beers[key].beers,
}));

(async function addCollectionAndDocuments(collectionKey, objectsToAdd) {
  try {
    const collectionRef = await getDocs(collection(db, collectionKey));
    if (collectionRef.empty) {
      const batch = writeBatch(db);
      objectsToAdd.forEach(async ({ title, beers }) => {
        const ref = doc(collection(db, collectionKey));
        batch.set(ref, { title, beers });
      });
      await batch.commit();
    }
    console.log('Process with no errors. Check firestore console');
    process.exit();
  } catch (error) {
    console.error(error.stack);
  }
})(
  'beers',
  collectionsArray.map(({ title, beers }) => ({ title, beers })),
);
