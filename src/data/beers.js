export default function beerData() {
  return {
    IPA: {
      id: 1,
      title: 'IPA',
      path: '/ipa',
      beers: [
        {
          id: '613c3958e621c561e303e97e',
          name: 'Insurgente Rompeolas',
          img: 'Rompeolas_Cerveceria_insurgentes_cervezas_Beerhouse.jpg',
          brand: 'Cervecería Insurgente',
          price: '70',
          style: 'IPA',
          sub_style: 'American IPA',
          alcohol_pct: '6',
        },
        {
          id: '613c3958e621c561e303e97f',
          name: 'Insurgente Lupulosa',
          img: 'Lupulosa_Cerveceria_insurgentes_cervezas_Beerhouse.jpg',
          brand: 'Cervecería Insurgente',
          price: '70',
          style: 'IPA',
          sub_style: 'American IPA',
          alcohol_pct: '7',
        },
        {
          id: '613c3958e621c561e303e980',
          name: 'Monstruo de Agua Blanca de Maguey',
          img: 'Monstruo-de-agua_blanca_magey_cervezas_Beerhouse.jpg',
          brand: 'Monstruo De Agua',
          price: '70',
          style: 'IPA',
          sub_style: 'White IPA',
          alcohol_pct: '7',
        },
        {
          id: '613c3958e621c561e303e981',
          name: 'Aguamala Astillero Lata',
          img: 'Agua_mala_astillero_cervezas_beerhouse.jpg',
          brand: 'Cervecería AguaMala',
          price: '60',
          style: 'IPA',
          sub_style: 'Double IPA',
          alcohol_pct: '7.2',
        },
        {
          id: '613c3958e621c561e303e982',
          name: 'Lefebvre Dikkenek',
          img: 'Dikkenek_Cervezas_Beerhouse.jpg',
          brand: 'Brasserie Lefebvre',
          price: '99',
          style: 'IPA',
          sub_style: 'Belgian IPA',
          alcohol_pct: '6.7',
        },
        {
          id: '613c3958e621c561e303e983',
          name: 'Lefebvre Funky Brett',
          img: 'Funky-Brell_Cervezas_Beerhouse.jpg',
          brand: 'Brasserie Lefebvre',
          price: '99',
          style: 'IPA',
          sub_style: 'Belgian IPA',
          alcohol_pct: '6.6',
        },
        {
          id: '613c3958e621c561e303e984',
          name: 'Loba Alfa',
          img: 'Loba_Alfa_cervezas_Beerhouse.jpg',
          brand: 'Loba',
          price: '57',
          style: 'IPA',
          sub_style: 'American IPA',
          alcohol_pct: '4.4',
        },
        {
          id: '613c3958e621c561e303e985',
          name: 'Colima Río de Lumbre',
          img: 'Colima_Riodelumbre_Cervezas_Beerhouse.jpg',
          brand: 'Cervecería De Colima',
          price: '53',
          style: 'IPA',
          sub_style: 'American IPA',
          alcohol_pct: '6.6',
        },
        {
          id: '613c3958e621c561e303e986',
          name: 'Wendlandt Tuna Turner',
          img: 'Tuna-turner_cervezas_beerhouse.jpg',
          brand: 'Wendlandt',
          price: '56',
          style: 'IPA',
          sub_style: 'Session IPA',
          alcohol_pct: '4',
        },
      ],
    },
    Porter: {
      id: 2,
      title: 'Porter',
      path: '/porter',
      beers: [
        {
          id: '613c3958e621c561e303e988',
          name: 'Cru Cru Porter',
          img: 'Cru-cru-Porter_1.png',
          brand: 'Cru Cru',
          price: '52',
          style: 'Porter',
          sub_style: 'English Porter',
          alcohol_pct: '6',
        },
        {
          id: '613c3958e621c561e303e989',
          name: 'La Brü Porter',
          img: 'Labru_porter_cerveza_Beerhouse.jpg',
          brand: 'La Brü',
          price: '50',
          style: 'Porter',
          sub_style: 'English Porter',
          alcohol_pct: '6.7',
        },
        {
          id: '613c3958e621c561e303e98a',
          name: 'Ticús',
          img: 'Colima_Ticus.png',
          brand: 'Cervecería De Colima',
          price: '49',
          style: 'Porter',
          sub_style: 'English Porter',
          alcohol_pct: '4.6',
        },
      ],
    },
    Weizen: {
      id: 3,
      title: 'Weizen',
      path: '/weizen',
      beers: [
        {
          id: '613c3958e621c561e303e98c',
          name: 'Hoegaarden',
          img: 'Hoegaarden-Botella.png',
          brand: 'Hoegaarden',
          price: '30',
          style: 'Weizen',
          alcohol_pct: '4.9',
        },
        {
          id: '613c3958e621c561e303e98d',
          name: 'Blanc 1664',
          img: 'Blanc_cerverzas_Beerhouse.jpg',
          brand: 'Blanc 1664',
          price: '41',
          style: 'Weizen',
        },
        {
          id: '613c3958e621c561e303e98e',
          name: 'Baja Brewing Mango Wheat',
          img: 'BajaBrewing_Mango_Cerveza_Beerhouse.png',
          brand: 'Baja Brewing Co.',
          price: '58',
          style: 'Weizen',
          sub_style: 'Fruit Beer',
          alcohol_pct: '5',
        },
        {
          id: '613c3958e621c561e303e98f',
          name: 'Cru Cru Berliner Weisse con Jamaica y Jengibre',
          img: 'crucruberliner.png',
          brand: 'Cru Cru',
          price: '53',
          style: 'Weizen',
          sub_style: 'Berliner Weisse',
          alcohol_pct: '3',
        },
        {
          id: '613c3958e621c561e303e990',
          name: 'Heroica Isla de Sacrificios',
          img: 'Heroica-Isla-sacrificios-Thumb.png',
          brand: 'Cervecería Heroica',
          price: '51',
          style: 'Weizen',
          sub_style: 'Weissbier',
          alcohol_pct: '4.1',
        },
        {
          id: '613c3958e621c561e303e991',
          name: 'Blanche de Bruges',
          img: 'BlanchedeBurgues_Brugs-Tarwebier_Single_Thumb_fe55d1f1-608f-4dee-8b92-df7184c43b6a.png',
          brand: 'Brouwerij De Halve Maan',
          price: '75',
          style: 'Weizen',
          sub_style: 'Witbier',
          alcohol_pct: '5',
        },
        {
          id: '613c3958e621c561e303e992',
          name: 'Paulaner Weissbier Dunkel',
          img: 'Paulaner_Weissbier_Dunkel_b41b63e8-8bd3-4518-92f2-3f79ceba7378.jpg',
          brand: 'Paulaner',
          price: '82',
          style: 'Weizen',
          sub_style: 'Dunkles Weissbier',
          alcohol_pct: '5.3',
        },
        {
          id: '613c3958e621c561e303e993',
          name: 'Paulaner Hefe-Weissbier',
          img: 'Paulaner_Weissbier_12205850-5bc1-4ed1-89af-e6bb5deff243.jpg',
          brand: 'Paulaner',
          price: '82',
          style: 'Weizen',
          sub_style: 'Weissbier',
          alcohol_pct: '5.5',
        },
        {
          id: '613c3958e621c561e303e994',
          name: 'Rrëy White',
          img: 'Rrey-Estilo-White.png',
          brand: 'Cervecería Rrëy',
          price: '42',
          style: 'Weizen',
          sub_style: 'Witbier',
          alcohol_pct: '4.9',
        },
      ],
    },
    Pilsner: {
      id: 4,
      title: 'Pilsner',
      path: '/pilsner',
      beers: [
        {
          id: '613c3958e621c561e303e996',
          name: 'Mexicali Sun Pilsner',
          img: 'Mexicali_Cerveza_Beerhouse.jpg',
          brand: 'Cervecería Mexicana',
          price: '28',
          style: 'Pilsner',
          sub_style: 'Lager Ligera',
          alcohol_pct: '4.5',
        },
        {
          id: '613c3958e621c561e303e997',
          name: 'Grolsch Swingtop',
          img: 'GPL_Swingtop_450ml.jpg',
          brand: 'GROLSCH',
          price: '56',
          style: 'Pilsner',
          sub_style: 'Czech Premium Pale Lager',
          alcohol_pct: '5',
        },
        {
          id: '613c3958e621c561e303e998',
          name: 'Baja La Surfa',
          img: 'Baja_surfa.png',
          brand: 'Baja Brewing Co.',
          price: '51',
          style: 'Pilsner',
          sub_style: 'German Pilsner',
          alcohol_pct: '4.5',
        },
        {
          id: '613c3958e621c561e303e999',
          name: 'Fauna Helium',
          img: 'Thumbnail-Single_helium-_1_1024x1024_e06954f3-4077-4a45-b15a-24d792e4d87a.jpg',
          brand: 'CERVEZA FAUNA',
          price: '41',
          style: 'Pilsner',
          sub_style: 'Mexican Pilsner',
          alcohol_pct: '5',
        },
        {
          id: '613c3958e621c561e303e99a',
          name: 'Flensburger Pilsener',
          img: 'FlensburgerPilsener_Thumb_388bd587-4f4b-4188-8f80-98bdff524761.png',
          brand: 'Flensburger Brauerei',
          price: '72',
          style: 'Pilsner',
          sub_style: 'German Pils',
          alcohol_pct: '4.8',
        },
        {
          id: '613c3958e621c561e303e99b',
          name: 'Cerveza Charro',
          img: 'Charro_botella_cervezas_Beerhouse.jpg',
          brand: 'Cervecería Charro',
          price: '49',
          style: 'Pilsner',
          alcohol_pct: '4.5',
        },
        {
          id: '613c3958e621c561e303e99c',
          name: 'Tijuana Güera',
          img: 'thumbnail_plp_tijuana_guera.jpg',
          brand: 'Cervecería Tijuana',
          price: '39',
          style: 'Pilsner',
          sub_style: 'Czech Premium Pale Lager',
          alcohol_pct: '4.6',
        },
        {
          id: '613c3958e621c561e303e99d',
          name: 'Ceiba Dorada Premium',
          img: 'Ceibadoradapremium.png',
          brand: 'Ceiba Cerveza Artesanal Yucateca',
          price: '50',
          style: 'Pilsner',
          sub_style: 'American Lager',
          alcohol_pct: '4.5',
        },
        {
          id: '613c3958e621c561e303e99e',
          name: 'Bocanegra Pilsner',
          img: 'Bocanegra_pilsner.png',
          brand: 'Cervecería Bocanegra',
          price: '32',
          style: 'Pilsner',
          alcohol_pct: '6',
        },
      ],
    },
    Stout: {
      id: 5,
      title: 'Stout',
      path: '/stout',
      beers: [
        {
          id: '613c3958e621c561e303e9a0',
          name: 'Guinness Draught',
          img: 'Guinness-Lata.png',
          brand: 'Guinness',
          price: '50',
          style: 'Stout',
          alcohol_pct: '4.2',
        },
        {
          id: '613c3958e621c561e303e9a1',
          name: 'Goose Island Bourbon County Stout',
          img: 'Goose-Island-Bourbon.png',
          brand: 'Goose Island Beer Company',
          price: '559',
          style: 'Stout',
          sub_style: 'American Stout',
          alcohol_pct: '13.8',
        },
        {
          id: '613c3958e621c561e303e9a2',
          name: 'La Brü Stout',
          img: 'La_Bru_Stout.jpg',
          brand: 'La Brü',
          price: '50',
          style: 'Stout',
          sub_style: 'Sweet Stout',
          alcohol_pct: '5',
        },
        {
          id: '613c3958e621c561e303e9a3',
          name: 'Baja Stout',
          img: 'Baja_Stout-Avena.png',
          brand: 'Baja Brewing Co.',
          price: '56',
          style: 'Stout',
          sub_style: 'Oatmeal Stout',
          alcohol_pct: '6',
        },
        {
          id: '613c3958e621c561e303e9a4',
          name: 'Minerva Stout Imperial',
          img: 'Minerva_Stout_Cervezas_Beerhouse.png',
          brand: 'Cervezería Minerva',
          price: '40',
          style: 'Stout',
          sub_style: 'Sweet Stout',
          alcohol_pct: '6',
        },
      ],
    },
  };
}
