import Catalogue from '../../components/Catalogue';

const HomePage = () => (
  <>
    <Catalogue />
  </>
);

export default HomePage;
