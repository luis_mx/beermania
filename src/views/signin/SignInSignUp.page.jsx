import SignIn from '../../components/SignIn';
import SignUp from '../../components/SignUp';
import { SignInSignUpStyles } from './SignInSignUp.styles';

const SignInSignUpPage = () => (
  <SignInSignUpStyles>
    <SignIn />
    <SignUp />
  </SignInSignUpStyles>
);

export default SignInSignUpPage;
