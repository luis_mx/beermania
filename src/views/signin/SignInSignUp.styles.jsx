import styled from '@emotion/styled';

export const SignInSignUpStyles = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(400px, 1fr));
  gap: 5vh 10vw;
`;
