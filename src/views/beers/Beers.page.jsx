import { Route, useRouteMatch } from 'react-router-dom';
import BeerCollectionsOverview from '../../components/BeerCollectionsOverview';
import BeerCollection from '../../components/BeerCollection';

const BeersPage = () => {
  const match = useRouteMatch();

  return (
    <>
      <Route exact path={match.path}>
        <BeerCollectionsOverview />
      </Route>
      <Route path={`${match.path}/:collectionId`}>
        <BeerCollection />
      </Route>
    </>
  );
};

export default BeersPage;
