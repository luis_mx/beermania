import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { collection, onSnapshot, query } from '@firebase/firestore';
import { convertBeersSnapshotToMap, db } from '../firebase';
import { updateBeers } from '../store/beers';

export default function useFetchCollections() {
  const dispatch = useDispatch();

  useEffect(() => {
    let unsubscribeFromSnapshot = null;
    (async function fetchData() {
      try {
        const collectionsQuery = await query(collection(db, 'beers'));
        unsubscribeFromSnapshot = await onSnapshot(collectionsQuery, (collectionsSnapshot) => {
          const beersMap = convertBeersSnapshotToMap(collectionsSnapshot);
          dispatch(updateBeers(beersMap));
        });
      } catch (error) {
        console.error(error.stack);
      }
    })();

    return function () {
      unsubscribeFromSnapshot();
    };
  }, [dispatch]);
}
