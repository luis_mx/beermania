import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { onAuthStateChanged } from '@firebase/auth';
import { onSnapshot } from '@firebase/firestore';
import { auth, createUserDocument } from '../firebase';
import { setCurrentUser } from '../store/user';

export default function useCurrentUser() {
  const dispatch = useDispatch();

  useEffect(() => {
    const unsubscribeFromAuth = onAuthStateChanged(
      auth,
      handleOnAuthStateChanged,
    );
    return () => unsubscribeFromAuth();

    async function handleOnAuthStateChanged(currentUser) {
      if (!currentUser) return dispatch(setCurrentUser(currentUser));
      const docRef = await createUserDocument(currentUser);
      try {
        await onSnapshot(docRef, (doc) =>
          dispatch(
            setCurrentUser({
              id: doc.id,
              ...doc.data(),
            }),
          ),
        );
      } catch (error) {
        console.error(error.stack);
      }
    }
  }, [dispatch]);
}
