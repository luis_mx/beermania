// https://firebase.google.com/docs/web/setup#available-libraries
import { initializeApp } from 'firebase/app';
import { getAuth, signInWithPopup, GoogleAuthProvider } from 'firebase/auth';
import { getFirestore, doc, getDoc, setDoc } from 'firebase/firestore';

const firebaseConfig = {
  apiKey: process.env.REACT_APP_APIKEY,
  authDomain: process.env.REACT_APP_AUTHDOMAIN,
  projectId: process.env.REACT_APP_PROJECTID,
  storageBucket: process.env.REACT_APP_STORAGEBUCKET,
  messagingSenderId: process.env.REACT_APP_MESSAGINGSENDERID,
  appId: process.env.REACT_APP_APPID,
};

initializeApp(firebaseConfig);

const db = getFirestore();
const auth = getAuth();
const provider = new GoogleAuthProvider();

provider.setCustomParameters({ prompt: 'select_account' });

const createUserDocument = async (userAuth, additionalData) => {
  if (!userAuth) return;
  try {
    const userRef = doc(db, 'users', userAuth.uid);
    const user = await getDoc(userRef);
    if (!user.exists()) {
      const { uid: id, displayName, email } = userAuth;
      const createdAt = new Date();

      await setDoc(doc(db, 'users', id), {
        email,
        displayName,
        createdAt,
        ...additionalData,
      });
    }
    return userRef;

    // const cartItems = await getDocs(collection(db, `users/${userAuth.uid}/cartItem`))
    // cartItems.forEach(doc=> console.log(`${doc.id} => ${JSON.stringify(doc.data())}`))
  } catch (error) {
    console.error(error.stack);
    throw error;
  }
};

const signInWithGoogle = async () => {
  try {
    const result = await signInWithPopup(auth, provider);
    await GoogleAuthProvider.credentialFromResult(result);
  } catch (error) {
    const credential = GoogleAuthProvider.credentialFromError(error);
    console.error(credential);
    throw error;
  }
};

export const convertBeersSnapshotToMap = (beers) =>
  beers.docs
    .map((beerStyle) => {
      const { title, beers } = beerStyle.data();
      return {
        routeName: encodeURI(title.toLowerCase()),
        id: beerStyle.id,
        title,
        beers,
      };
    })
    .reduce(
      (map, collection) => ({
        ...map,
        [collection.title.toLowerCase()]: collection,
      }),
      {},
    );

export { createUserDocument, signInWithGoogle, auth, db };
