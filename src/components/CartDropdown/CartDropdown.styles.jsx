import styled from '@emotion/styled';

export const CartDropdownStyles = styled.div`
  --padding: 0.8rem;
  position: absolute;
  width: 260px;
  height: 350px;
  display: flex;
  flex-direction: column;
  padding: var(--padding);
  border: 1px solid #000;
  background-color: #fff;
  top: 80px;
  right: 0;
  z-index: 2;

  button {
    margin-top: auto;
  }
`;

export const CartItems = styled.div`
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  overflow: scroll;
  margin-bottom: var(--padding);
`;
