import { useEffect, useRef } from 'react';
import { useSelector } from 'react-redux';
import gsap from 'gsap';
import { CartDropdownStyles, CartItems } from './CartDropdown.styles';
import Button from '../Button';
import CartItem from '../CartItem';

const CartDropdown = () => {
  const cartItems = useSelector(({ cart }) => cart.cartItems);

  const el = useRef();
  useEffect(() =>
    gsap.to(el.current, { autoAlpha: 1, ease: 'none', duration: 0.25 }),
  );

  return (
    <CartDropdownStyles data-hidden ref={el}>
      <CartItems>
        {cartItems.map((item) => (
          <CartItem key={item.id} item={item} />
        ))}
      </CartItems>
      <Button noPx>Go to checkout</Button>
    </CartDropdownStyles>
  );
};

export default CartDropdown;
