import { useHistory, useRouteMatch } from 'react-router-dom';
import { ItemStyles, TitleStyles } from './CatalogueItem.styles';

const BeerOverviewItem = ({ collection }) => {
  const { title, img, path } = collection;

  const history = useHistory();
  const match = useRouteMatch();

  const handleClick = () => history.push(`${match.url}beers${path}`);

  return (
    <ItemStyles title={title} onClick={handleClick}>
      <img src={`/images/beer-overview/${img}`} alt="" />
      <TitleStyles>{title}</TitleStyles>
    </ItemStyles>
  );
};

export default BeerOverviewItem;
