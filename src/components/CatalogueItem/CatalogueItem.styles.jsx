import styled from '@emotion/styled';

export const ItemStyles = styled.div`
  grid-area: ${({ title }) => title};
  display: flex;
  justify-content: center;
  align-items: center;
  height: 25vh;
  height: min(25vh, 400px);
  min-height: 250px;
  cursor: pointer;
  position: relative;
  overflow: hidden;
  border-radius: 4px;
  box-shadow: 0 2px 8px rgba(0, 0, 0, 0.08);

  &:hover {
    img {
      transform: scale(1.1);
      opacity: 0.9;
    }
  }

  img {
    position: absolute;
    width: 100%;
    height: 100%;
    object-fit: cover;
    object-position: center;
    transform: scale(1);
    will-change: transform;
    transition: transform 0.5s ease-in-out;
  }
`;

export const TitleStyles = styled.span`
  background-color: rgba(255, 255, 255, 0.6);
  backdrop-filter: blur(5px);
  border: 1px solid var(--d_orange);
  padding: 0.75rem 3rem;
  font-size: 1.5rem;
  border-radius: 16px 8px 16px 8px;
  font-weight: 700;
`;
