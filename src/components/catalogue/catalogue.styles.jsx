/** @jsxImportSource @emotion/react */
import styled from '@emotion/styled';

export const CatalogueStyles = styled.div`
  display: grid;
  grid-template-areas: 'IPA IPA Porter Porter Weizen Weizen' 'Pilsner Pilsner Pilsner Stout Stout Stout';
  gap: 3vh;
`;
