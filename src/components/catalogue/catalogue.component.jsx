import { useEffect, useMemo, useRef } from 'react';
import { useSelector } from 'react-redux';
import gsap from 'gsap';

import { selectCatalogue } from '../../store/catalogue';
import { CatalogueStyles } from './Catalogue.styles';
import CatalogueItem from '../CatalogueItem';

const Catalogue = () => {
  const catalogue = useSelector(selectCatalogue);

  const catalogueEl = useRef();
  const itemElems = useMemo(() => gsap.utils.selector(catalogueEl), []);

  useEffect(() => {
    gsap
      .timeline({ delay: 0.3, defaults: { ease: 'power1.inOut', duration: 0.4 } })
      .set(catalogueEl.current, { autoAlpha: 1 })
      .from(itemElems('div'), { autoAlpha: 0, scale: 0.9, stagger: 0.02 });
  }, [itemElems]);

  return (
    <CatalogueStyles data-hidden ref={catalogueEl}>
      {catalogue?.length
        ? catalogue.map((collection) => (
            <CatalogueItem key={collection.id} collection={collection} />
          ))
        : null}
    </CatalogueStyles>
  );
};

export default Catalogue;
