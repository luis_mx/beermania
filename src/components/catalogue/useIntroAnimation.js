import gsap from 'gsap';
import { useEffect } from 'react';

export const useIntroAnimation = ({ catalogueEl, itemElems }) => {
  useEffect(() => {
    gsap
      .timeline({ delay: 0.3, defaults: { ease: 'power1.out', duration: 0.4 } })
      .set(catalogueEl.current, { autoAlpha: 1 })
      .from(itemElems('div'), { autoAlpha: 0, scale: 0.9, stagger: 0.15 });
  }, [catalogueEl, itemElems]);
};
