import { createStructuredSelector } from '@reduxjs/toolkit/node_modules/reselect';
import { useSelector } from 'react-redux';
import { selectCollectionsForPreview } from '../../store/beers';
import BeerCollectionPreview from '../BeerCollectionPreview';

const selectors = createStructuredSelector({
  collections: selectCollectionsForPreview,
});

const BeerCollectionsOverview = () => {
  const { collections } = useSelector(selectors);

  return !!collections?.length
    ? collections.map((collection) => (
        <BeerCollectionPreview key={collection.id} collection={collection} />
      ))
    : null;
};

export default BeerCollectionsOverview;
