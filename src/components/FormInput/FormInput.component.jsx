import { InputStyles } from './FormInput.styles';

const FormInput = ({ onChange, label, ...props }) => (
  <InputStyles shrink={!!props.value.length}>
    <input onChange={onChange} {...props} />
    <label>{label}</label>
  </InputStyles>
);

export default FormInput;
