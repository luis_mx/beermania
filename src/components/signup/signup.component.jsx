import { Component } from 'react';
import { createUserWithEmailAndPassword } from 'firebase/auth';
import { auth, createUserDocument } from '../../firebase';

import { SignStyles } from '../Sign/Sign.styles';
import Button from '../Button';
import FormInput from '../FormInput';
import FormError from '../FormError';

const INITSTATE = {
  displayName: '',
  email: '',
  password: '',
  repeatPassword: '',
  authError: '',
  disableForm: false,
};

class SignUp extends Component {
  constructor() {
    super();
    this.state = { ...INITSTATE };
  }

  handleSubmit = async (evt) => {
    evt.preventDefault();
    this.setState({ disableForm: true });
    const { displayName, email, password, repeatPassword } = this.state;

    if (password !== repeatPassword) {
      const errorMessage = `Passwords don't match`;
      return this.setState({
        disableForm: false,
        password: '',
        repeatPassword: '',
        authError: errorMessage,
      });
    }

    if (password.length < 6) {
      const errorMessage = `Password must be at least 6 characters long`;
      return this.setState({
        disableForm: false,
        password: '',
        repeatPassword: '',
        authError: errorMessage,
      });
    }

    try {
      const { user } = await createUserWithEmailAndPassword(
        auth,
        email,
        password,
      );
      await createUserDocument(user, { displayName });

      this.setState({ ...INITSTATE });
    } catch (error) {
      const {
        groups: { authError },
      } = error.message.match(/\(auth\/(?<authError>.*)\)/);

      if (authError === 'email-already-in-use')
        return this.setState({
          email: '',
          disableForm: false,
          authError: 'Email already registered',
        });

      console.error(error.stack);
    }
  };

  handleChange = (evt) => {
    const { name, value } = evt.target;
    this.setState({ [name]: value, authError: '' });
  };

  render() {
    const {
      displayName,
      email,
      password,
      repeatPassword,
      authError,
      disableForm,
    } = this.state;

    return (
      <SignStyles>
        <h2>I don't have an account</h2>
        <span>Sign Up with your email and password</span>

        <FormError authError={authError} />

        <form onSubmit={this.handleSubmit}>
          <FormInput
            onChange={this.handleChange}
            label="Display name"
            type="text"
            name="displayName"
            value={displayName}
            required
          />
          <FormInput
            onChange={this.handleChange}
            label="Email"
            type="email"
            name="email"
            value={email}
            autoComplete="username"
            required
          />
          <FormInput
            onChange={this.handleChange}
            label="Password"
            type="password"
            name="password"
            value={password}
            autoComplete="new-password"
            required
          />
          <FormInput
            onChange={this.handleChange}
            label="Repeat password"
            type="password"
            name="repeatPassword"
            value={repeatPassword}
            autoComplete="new-password"
            required
          />

          <Button dark disable={disableForm.toString()} type="submit">
            Sign Up
          </Button>
        </form>
      </SignStyles>
    );
  }
}

export default SignUp;
