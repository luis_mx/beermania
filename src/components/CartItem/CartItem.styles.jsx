import styled from '@emotion/styled';

export const CartItemStyles = styled.div`
  width: 100%;
  display: flex;
  height: 80px;
  /* margin-bottom: 16px; */
  font-size: 0.625rem;

  img {
    width: 30%;
    object-fit: contain;
  }
  div {
    flex-grow: 1;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: center;
    padding: 8px 4px;
  }
`;

export const CartItemNameStyles = styled.span`
  font-size: 0.8rem;
`;
