import { CartItemNameStyles, CartItemStyles } from './CartItem.styles';

const CartItem = ({ item }) => {
  const { name, img, price, quantity } = item;

  return (
    <CartItemStyles>
      <img src={img} alt="" />
      <div>
        <CartItemNameStyles>{name}</CartItemNameStyles>
        <span>
          {quantity} &times; ${price}
        </span>
      </div>
    </CartItemStyles>
  );
};

export default CartItem;
