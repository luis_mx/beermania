import { Route, Redirect, Switch } from 'react-router-dom';
import { useSelector } from 'react-redux';

import './App.css';
import { useCurrentUser, useFetchCollections } from '../../hooks';
import Layout from '../Layout';
import BeersPage from '../../views/beers';
import HomePage from '../../views/home';
import SignInSignUpPage from '../../views/signin';

function App() {
  const currentUser = useSelector((state) => state.user.currentUser);

  useCurrentUser();
  useFetchCollections();

  return (
    <Layout>
      <Switch>
        <Route exact path="/">
          <HomePage />
        </Route>
        <Route path="/beers">
          <BeersPage />
        </Route>
        {!currentUser && (
          <Route path="/signin">
            <SignInSignUpPage />
          </Route>
        )}
        <Route path="*">
          <Redirect to="/" />
        </Route>
      </Switch>
    </Layout>
  );
}

export default App;
