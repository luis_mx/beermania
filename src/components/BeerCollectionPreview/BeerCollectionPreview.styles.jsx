import styled from '@emotion/styled';

export const BeerCollectionStyles = styled.section`
  padding-top: 2vh;
  padding-bottom: 2vh;

  &:last-child {
    padding-bottom: 5vh;
  }
`;

export const BeerItemsWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  align-items: start;
  gap: 2vw;

  @media (max-width: 768px) {
    grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
  }
`;

export const Title = styled.h1`
  font-size: 3vh;
  font-size: max(3vh, 1.2rem);

  a {
    position: relative;

    &::after {
      content: '';
      position: absolute;
      top: 100%;
      left: 0;
      background-color: var(--dark);
      height: 3px;
      width: 100%;
      transform: scaleX(0);
      transform-origin: left;
      transition: transform .25s ease-out;
    }

    &:hover::after {
      transform: scaleX(1);
    }
  }
`;
