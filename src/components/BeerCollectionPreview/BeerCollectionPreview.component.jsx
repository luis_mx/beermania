import { useRouteMatch, Link } from 'react-router-dom';
import BeerItem from '../BeerItem';
import {
  BeerCollectionStyles,
  BeerItemsWrapper,
  Title,
} from './BeerCollectionPreview.styles';

export default function BeerCollectionPreview({ collection }) {
  const { routeName, title, beers } = collection;
  const match = useRouteMatch();

  return (
    <BeerCollectionStyles>
      <Title>
        <Link to={`${match.path}/${routeName}`}>{title}</Link>
      </Title>
      <BeerItemsWrapper>
        {!!beers?.length
          ? beers.map((beer) => (
              <BeerItem key={beer.id} beer={beer} routeName={routeName} />
            ))
          : null}
      </BeerItemsWrapper>
    </BeerCollectionStyles>
  );
}
