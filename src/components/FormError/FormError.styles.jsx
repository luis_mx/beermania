import styled from '@emotion/styled';

export const FormErrorStyles = styled.div`
  p {
    margin-bottom: 0;
    font-weight: 700;

    span {
      display: inline-block;

      &:first-of-type {
        color: red;
      }
      &:last-of-type {
        border-bottom: 2px solid red;
      }
    }
  }
`;
