import { FormErrorStyles } from './FormError.styles';

const FormError = ({ authError }) => (
  <FormErrorStyles>
    <p>
      {!!authError ? (
        <>
          <span>*</span> <span>{authError}</span>
        </>
      ) : (
        '\u00a0' // &nbsp;
      )}
    </p>
  </FormErrorStyles>
);

export default FormError;
