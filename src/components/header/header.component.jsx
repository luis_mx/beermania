import { useEffect, useMemo, useRef } from 'react';
import { Link } from 'react-router-dom';
import { signOut } from 'firebase/auth';
import { useSelector } from 'react-redux';
import gsap from 'gsap';
import { auth } from '../../firebase';

import logo from '../../assets/logo.png';
import { HeaderStyles, LogoStyles, NavStyles } from './Header.styles';
import CartIcon from '../CartIcon';
import CartDropdown from '../CartDropdown';

async function handleSignOut() {
  try {
    await signOut(auth);
  } catch (error) {
    console.error(error.stack);
    throw error;
  }
}

const Header = () => {
  const { currentUser, cartIsHidden } = useSelector(({ user, cart }) => ({
    currentUser: user.currentUser,
    cartIsHidden: cart.cartIsHidden,
  }));

  const headerEl = useRef();
  const q = useMemo(() => gsap.utils.selector(headerEl), []);

  useEffect(() => {
    gsap
      .timeline({
        delay: 0.3,
        defaults: { ease: 'power1.inOut', duration: 0.33 },
      })
      .set(headerEl.current, { autoAlpha: 1 })
      .from(q('h1'), { x: '-=10vw', autoAlpha: 0 })
      .from(q('nav > div'), { x: '+=5vw', stagger: 0.1, autoAlpha: 0 }, '<')
      .from(
        q('nav > div'),
        { y: '-=2vh', stagger: 0.15, ease: 'power2.in' },
        '<',
      );
  }, [q]);

  return (
    <HeaderStyles data-hidden ref={headerEl}>
      <LogoStyles>
        <Link to="/">
          <img src={logo} alt="" />
        </Link>
      </LogoStyles>
      <NavStyles>
        <div>
          <Link to="/beers">Beers</Link>
        </div>
        {currentUser ? (
          <div>
            <button
              style={{ backgroundColor: 'transparent' }}
              onClick={handleSignOut}
            >
              Sign Out
            </button>
          </div>
        ) : (
          <div>
            <Link to="/signin">Sign In</Link>
          </div>
        )}
        <CartIcon />
      </NavStyles>
      {!cartIsHidden && <CartDropdown />}
    </HeaderStyles>
  );
};

export default Header;
