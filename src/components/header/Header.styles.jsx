import styled from '@emotion/styled';

export const HeaderStyles = styled.header`
  height: 5rem;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 2vh;
  position: relative;
`;

export const LogoStyles = styled.h1`
  margin: 0;
  height: 70%;

  img {
    max-height: 100%;
  }
`;

export const NavStyles = styled.nav`
  display: flex;
  align-items: center;
  gap: 3vw;

  a {
    padding: 1rem;
  }
`;
