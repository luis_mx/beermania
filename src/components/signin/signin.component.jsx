import { useState } from 'react';
import { signInWithEmailAndPassword } from 'firebase/auth';
import { auth, signInWithGoogle } from '../../firebase';

import { SignStyles } from '../Sign/Sign.styles';
import Button from '../Button';
import FormInput from '../FormInput';
import FormError from '../FormError';

function SignIn() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [authError, setAuthError] = useState('');
  const [disableForm, setDisableForm] = useState(false);

  const stateSetter = {
    email: setEmail,
    password: setPassword,
  };

  const stateReset = ({ email = '', error = '' }) => {
    setEmail(email);
    setAuthError(error);
    setPassword('');
    setDisableForm(false);
  };

  const handleSubmit = async (evt) => {
    evt.preventDefault();
    setDisableForm(true);

    try {
      await signInWithEmailAndPassword(auth, email, password);
      stateReset({ email: '' });
    } catch (error) {
      const { groups } = error.message.match(/\(auth\/(?<authError>.*)\)/);

      if (groups.authError === 'wrong-password') {
        return stateReset({ email, error: 'Wrong credentials' });
      }
      if (groups.authError === 'user-not-found') {
        return stateReset({ error: 'Email not registered' });
      }
      if (groups.authError === 'too-many-requests') {
        return stateReset({
          error: `We couldn't process your sign in. Try again later`,
        });
      }
      console.error(error.stack);
    }
  };

  const handleChange = (evt) => {
    const { name, value } = evt.target;
    stateSetter[name](value);
    setAuthError('');
  };

  return (
    <SignStyles>
      <h2>I already have an account</h2>
      <span>Sign in with your email and password</span>

      <FormError {...{ authError }} />

      <form onSubmit={handleSubmit}>
        <FormInput
          onChange={handleChange}
          label="Email"
          type="email"
          name="email"
          value={email}
          autoComplete="username"
          required
        />
        <FormInput
          onChange={handleChange}
          label="Password"
          type="password"
          name="password"
          value={password}
          autoComplete="current-password"
          required
        />
        <Button dark disable={disableForm.toString()} type="submit">
          Sign In
        </Button>
        <Button
          onClick={signInWithGoogle}
          blue
          disable={disableForm.toString()}
          type="buton"
        >
          Sign In With Google
        </Button>
      </form>
    </SignStyles>
  );
}

export default SignIn;
