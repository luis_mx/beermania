import { useDispatch } from 'react-redux';
import { addItemToCart } from '../../store/cart';
import {
  BeerItemButton,
  BottomBox,
  ItemStyles,
  Name,
  Price,
} from './BeerItem.styles';

const BeerItem = ({ beer, routeName }) => {
  const { name, price, img } = beer;

  const imgUrl = `/images/beers/${routeName}/${img}`;

  const dispatch = useDispatch();
  const handleAddItemToCart = () =>
    dispatch(addItemToCart({ ...beer, img: imgUrl }));

  return (
    <ItemStyles>
      <img src={imgUrl} alt="" />
      <Price>$&thinsp;{price}</Price>
      <BottomBox>
        <Name title={name}>{name}</Name>
        <BeerItemButton onClick={handleAddItemToCart}>
          Add to Cart
        </BeerItemButton>
      </BottomBox>
    </ItemStyles>
  );
};

export default BeerItem;
