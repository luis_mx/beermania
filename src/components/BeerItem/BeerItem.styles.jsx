import styled from '@emotion/styled';
import Button from '../Button';

export const ItemStyles = styled.div`
  position: relative;
  box-shadow: 0 3px 12px rgba(0, 0, 0, 0.066);
  border: 1px solid rgba(0, 0, 0, 0.1);
  border-radius: 8px;
  overflow: hidden;
  background-color: #fff;
  cursor: pointer;

  &:hover {
    button {
      transform: translateY(0vh);
      opacity: 1;
    }
    img {
      opacity: 0.9;
    }
  }

  span {
    display: block;
    text-align: center;
  }

  img {
    height: 340px;
    width: 100%;
    object-fit: contain;
    object-position: center;
  }
`;
export const BottomBox = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
`;
export const Name = styled.span`
  text-align: center;
  padding: 1rem;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  width: 100%;
  position: relative;
  z-index: 1;
  background-color: var(--yellow);

  &:hover {
    background-color: var(--d_yellow);
  }
`;
export const Price = styled.span`
  position: absolute;
  right: 0;
  top: 0;
  background-color: var(--l_orange);
  padding: 0.5rem 1rem;
  border-radius: 0 0 0 12px;
`;
export const BeerItemButton = styled(Button)`
  position: absolute;
  bottom: calc(100% + 2vh);
  transform: translateY(3vh);
  opacity: 0;
`;
