import { LayoutStyles } from './Layout.styles';
import Header from '../Header';

export default function Layout({ children }) {
  return (
    <LayoutStyles>
      <Header />
      <main>{children}</main>
    </LayoutStyles>
  );
}
