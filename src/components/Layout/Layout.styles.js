import styled from '@emotion/styled';

export const LayoutStyles = styled.section`
  height: 100%;
  padding: 0 3vw 3vw;
  display: flex;
  flex-direction: column;
`;