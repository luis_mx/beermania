import { ButtonStyles } from './Button.styles';

const Button = ({ children, type = 'button', ...props }) => (
  <ButtonStyles type={type} {...props}>
    {children}
  </ButtonStyles>
);

export default Button;
