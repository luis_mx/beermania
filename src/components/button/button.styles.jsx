import styled from '@emotion/styled';
import { css } from '@emotion/react';

export const ButtonStyles = styled.button`
  padding: 0.5rem ${(props) => (props.noPx ? '0' : '2rem')};
  border-radius: 20px;
  transition: all 0.25s ease-in-out;
  font-size: 0.75rem;
  font-weight: 700;
  text-transform: uppercase;
  border: 1px solid transparent;

  button ~ & {
    margin-left: 1rem;
  }

  ${invokeButtonStyles}
`;

function invokeButtonStyles({ dark, blue }) {
  if (dark) return darkStyles;
  if (blue) return blueStyles;
  return greenHoverStyles;
}

const darkStyles = css`
  background-color: var(--dark);
  color: white;

  &:hover {
    border-color: var(--dark);
    background-color: var(--l_grey);
    color: var(--dark);
  }
`;

const blueStyles = css`
  background-color: #4285f4;
  color: white;

  &:hover {
    background-color: #357ae8;
    border-color: #357ae8;
  }
`;

const greenHoverStyles = css`
  &:hover {
    background-color: var(--green);
    color: white;
  }
`;
