import { useDispatch, useSelector } from 'react-redux';
import { selectCartItemsCount, toggleCartVisibility } from '../../store/cart';
import { ReactComponent as Icon } from '../../assets/shopping-bag.svg';
import { CartIconStyles } from './CartIcon.styles';

const CartIcon = () => {
  const itemsCount = useSelector(selectCartItemsCount);

  const dispatch = useDispatch();
  const handleToggleCartVisibility = () => dispatch(toggleCartVisibility());

  return (
    <CartIconStyles onClick={handleToggleCartVisibility}>
      <Icon />
      <span>{itemsCount}</span>
    </CartIconStyles>
  );
};

export default CartIcon;
