import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { selectCollection } from '../../store/beers';
import BeerItem from '../BeerItem';
import {
  BeerCollectionStyles,
  BeerItemsWrapper,
} from './BeerCollection.styles';

export default function BeerCollection() {
  const { collectionId } = useParams();

  const { collection } = useSelector((state) => ({
    collection: selectCollection(collectionId)(state),
  }));

  return collection ? (
    <BeerCollectionStyles>
      <h1>{collection.title}</h1>
      <BeerItemsWrapper>
        {collection.beers
          ? collection.beers.map((beer) => (
              <BeerItem
                key={beer.id}
                beer={beer}
                routeName={collection.routeName}
              />
            ))
          : null}
      </BeerItemsWrapper>
    </BeerCollectionStyles>
  ) : null;
}
