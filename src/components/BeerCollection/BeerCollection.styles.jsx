import styled from '@emotion/styled';

export const BeerCollectionStyles = styled.section`
  padding-top: 2vh;
  padding-bottom: 2vh;

  &:last-child {
    padding-bottom: 5vh;
  }
`;

export const BeerItemsWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  align-items: start;
  gap: 2vw;

  @media (max-width: 768px) {
    grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
  }
`;
