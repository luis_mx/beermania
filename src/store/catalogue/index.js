import { createSelector } from 'reselect';

export const selectCatalogue = createSelector(
  (state) => state.catalogue,
  (catalogue) => catalogue.sections,
);
