import catalogueData from '../../data/beer.catalogue.data.json';

const INITIAL_STATE = {
  sections: catalogueData,
}

const catalogueReducer = (state = INITIAL_STATE, action) => {
  switch (action.payload) {
    default:
      return state;
  }
};

export default catalogueReducer;
