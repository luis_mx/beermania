import { combineReducers, createStore, applyMiddleware } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import logger from 'redux-logger';

import userReducer from './user/user.reducer';
import cartReducer from './cart/cart.reducer';
import catalogueReducer from './catalogue/catalogue.reducer';
import beersReducer from './beers/beers.reducer';

// REDUCERs
const rootReducer = combineReducers({
  user: userReducer,
  cart: cartReducer,
  catalogue: catalogueReducer,
  beers: beersReducer,
});

const PERSIST_CONFIG = {
  key: 'root',
  storage,
  whitelist: ['cart'],
};

// MIDDLEWARES
const middleware = [];
if (process.env.NODE_ENV === 'development') middleware.push(logger);

// STORE CONFIG
export const store = createStore(
  persistReducer(PERSIST_CONFIG, rootReducer),
  applyMiddleware(...middleware),
);

export const persistor = persistStore(store);
