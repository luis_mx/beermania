const BeersActionTypes = {
  UPDATE_BEERS: 'UPDATE_BEERS',
};

export default BeersActionTypes;
