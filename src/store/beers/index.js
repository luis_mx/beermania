import { createSelector } from 'reselect';
import memoize from 'lodash.memoize';
import BeersActionTypes from './beers.types';

export const updateBeers = (beers) => ({
  type: BeersActionTypes.UPDATE_BEERS,
  payload: beers,
});

const selectBeers = (state) => state.beers;

export const selectBeersCollections = createSelector(
  [selectBeers],
  (beers) => beers.collections,
);

export const selectCollectionsForPreview = createSelector(
  [selectBeersCollections],
  (collections) =>
    collections
      ? Object.keys(collections).map((key) => ({
          ...collections[key],
          beers: collections[key].beers.slice(0, 4),
        }))
      : [],
);

export const selectCollection = memoize((collectionUrlParam) =>
  createSelector([selectBeersCollections], (collections) =>
    collections ? collections[collectionUrlParam] : null,
  ),
);
