import BeersActionTypes from './beers.types';

const INITIAL_STATE = {
  collections: {},
};

const beersReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case BeersActionTypes.UPDATE_BEERS:
      return {
        ...state,
        collections: action.payload,
      };
    default:
      return state;
  }
};

export default beersReducer;
