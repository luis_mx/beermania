import { createSelector } from 'reselect';
import CartActionTypes from './cart.types';

// ACTIONS
export const toggleCartVisibility = () => ({
  type: CartActionTypes.TOGGLE_CART,
});

export const addItemToCart = (item) => ({
  type: CartActionTypes.ADD_ITEM,
  payload: item,
});

// SELECTORS
const selectCart = (state) => state.cart;

export const selectCartIsHidden = createSelector(
  [selectCart],
  (cart) => cart.cartIsHidden,
);

export const selectCartItems = createSelector(
  [selectCart],
  (cart) => cart.cartItems,
);

export const selectCartItemsCount = createSelector(
  [selectCartItems],
  (cartItems) => cartItems.reduce((count, { quantity }) => count + quantity, 0),
);

