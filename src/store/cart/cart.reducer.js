import CartActionTypes from './cart.types';
import { addItemToCart } from './cart.utils';

const INITIAL_STATE = {
  cartIsHidden: true,
  cartItems: [],
};

const cartReducer = (state = INITIAL_STATE, action) => {
  // const { payload: item } = action;

  switch (action.type) {
    case CartActionTypes.TOGGLE_CART:
      return {
        ...state,
        cartIsHidden: !state.cartIsHidden,
      };
    case CartActionTypes.ADD_ITEM:
      return {
        ...state,
        cartItems: addItemToCart(state.cartItems, action.payload),
        // cartItems: addItemToCart(state.cartItems, item),
      };
    default:
      return state;
  }
};

export default cartReducer;
